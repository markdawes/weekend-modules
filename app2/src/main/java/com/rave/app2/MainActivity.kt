package com.rave.app2

import android.os.Bundle
import androidx.fragment.app.FragmentActivity

/**
 * Entry point for module 2.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
