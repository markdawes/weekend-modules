package com.rave.app3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.rave.app3.ui.theme.WeekendModulesTheme

/**
 * Second fragment.
 *
 * @constructor Create empty Second fragment
 */
class SecondFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                WeekendModulesTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = Color.Red
                    ) {
                        Text(text = "Second Fragment")
                        Column(
                            modifier = Modifier.fillMaxSize(),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Row {
                                Button(onClick = {
                                    findNavController().navigate(R.id.action_secondFragment_to_firstFragment)
                                }, modifier = Modifier.requiredSize(150.dp)) {
                                    Text(text = "Prev")
                                }
                                Button(onClick = {
                                    findNavController().navigate(R.id.action_secondFragment_to_thirdFragment)
                                }, modifier = Modifier.requiredSize(150.dp)) {
                                    Text(text = "Next")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
