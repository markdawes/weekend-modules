package com.rave.app3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.rave.app3.ui.theme.WeekendModulesTheme

/**
 * Fifth fragment.
 *
 * @constructor Create empty Fifth fragment
 */
class FifthFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                WeekendModulesTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = Color.Magenta
                    ) {
                        Text(text = "Fifth Fragment")
                        Column(
                            modifier = Modifier.fillMaxSize(),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Button(onClick = {
                                findNavController().navigate(R.id.action_fifthFragment_to_fourthFragment)
                            }, modifier = Modifier.requiredSize(150.dp)) {
                                Text(text = "Prev")
                            }
                        }
                    }
                }
            }
        }
    }
}
