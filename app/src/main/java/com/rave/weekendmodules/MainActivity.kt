package com.rave.weekendmodules

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment

/**
 * Entry point for module 1.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : FragmentActivity() {

    @Suppress("LateinitUsage")
    private lateinit var navController: NavController
    private val tag = "MyActivity"

    /**
     * OnCreate() being logged.
     *
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(tag, "onCreate() called")
        setContentView(R.layout.activity_main)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
    }

    /**
     * OnStart() being logged.
     */
    override fun onStart() {
        super.onStart()
        Log.i(tag, "onStart() called")
    }

    /**
     * OnResume() being logged.
     */
    override fun onResume() {
        super.onResume()
        Log.i(tag, "onResume() called")
    }

    /**
     * OnPause() being logged.
     */
    override fun onPause() {
        super.onPause()
        Log.i(tag, "onPause() called")
    }

    /**
     * OnStop() being logged.
     */
    override fun onStop() {
        super.onStop()
        Log.i(tag, "onStop() called")
    }

    /**
     * OnRestart() being logged.
     */
    override fun onRestart() {
        super.onRestart()
        Log.i(tag, "onRestart() called")
    }

    /**
     * OnDestroy() being logged.
     */
    override fun onDestroy() {
        super.onDestroy()
        Log.i(tag, "onDestroy() called")
    }
}
