package com.rave.weekendmodules

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material3.Text
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import com.rave.weekendmodules.ui.theme.WeekendModulesTheme

/**
 * Main fragment.
 *
 * @constructor Create empty Main fragment
 */
class MainFragment : Fragment() {

    private val fragmenttag = "Main Fragment"

    /**
     * OnCreate() being logged.
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(fragmenttag, "onCreate() called")
    }

    /**
     * On CreateView() being logged.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                WeekendModulesTheme {
                    Log.i(fragmenttag, "onCreateView() called")
                    Text("Fragment loading")
                }
            }
        }
    }

    /**
     * onViewCreated() being logged.
     *
     * @param view
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(fragmenttag, "onViewCreated() called")
    }

    /**
     * onViewStateRestored Called.
     *
     * @param savedInstanceState
     */
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        Log.i(fragmenttag, "onViewStateRestored() called")
    }

    /**
     * onStart() being logged.
     *
     */
    override fun onStart() {
        super.onStart()
        Log.i(fragmenttag, "onStart() being called")
    }

    /**
     * onResume() being logged.
     *
     */
    override fun onResume() {
        super.onResume()
        Log.i(fragmenttag, "onResume() being called")
    }

    /**
     * onPause() being logged.
     *
     */
    override fun onPause() {
        super.onPause()
        Log.i(fragmenttag, "onPause() being called")
    }

    /**
     * onStop() being logged.
     *
     */
    override fun onStop() {
        super.onStop()
        Log.i(fragmenttag, "onStop() being called")
    }

    /**
     * onSaveInstance() being logged.
     *
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.i(fragmenttag, "onSaveInstance() being called")
    }

    /**
     * onDestroyView() being logged.
     *
     */
    override fun onDestroyView() {
        super.onDestroyView()
        Log.i(fragmenttag, "onDestroyView() being called")
    }

    /**
     * onDestroy() being logged.
     *
     */
    override fun onDestroy() {
        super.onDestroy()
        Log.i(fragmenttag, "onDestroy() being called")
    }
}
